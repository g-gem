require 'rubygems'

class Portage
  class << self
    attr_reader :overlay_dirs, :portage_dir, :portage_distdir, :portage_dev_ruby, :category
    attr_reader :rubydev_overlay

    def init(path="/etc/make.conf")
      @path = path
      @overlay_dirs = %x(/usr/lib/portage/bin/portageq portdir_overlay).chomp.split(/ +/)
      @portage_dir = %x(/usr/lib/portage/bin/portageq portdir).chomp
      @portage_distdir = %x(/usr/lib/portage/bin/portageq distdir).chomp
      @category = 'dev-ruby'
      @ruby_vdb = File.join(%x(/usr/lib/portage/bin/portageq distdir).chomp,@category)
      @portage_dir = '/usr/portage' unless File.directory? @portage_dir
      @portage_distdir = '/usr/portage/distfiles' unless File.directory? @portage_distdir
      @portage_dev_ruby = File.join(@portage_dir,@category)

      @tmp_overlay_dir = "/tmp/ruby-modules_#{Process.pid}"
      @overlay_dir = @overlay_dirs.find {|o| File.directory? o}
    end

    def setup_overlay(tmp_overlay = false)
      @overlay_dir = (tmp_overlay == false && @overlay_dir) || @tmp_overlay_dir
      Dir.mkdir(@overlay_dir, 0755) unless File.directory? @overlay_dir
      @rubydev_overlay = File.join(@overlay_dir,@category)
      Dir.mkdir(@rubydev_overlay, 0755) unless File.directory? @rubydev_overlay
      ENV['PORTDIR_OVERLAY'] = @overlay_dir
    end

    def cleanup
      # Should strictly check to make sure we don't accidentally rm_rf $HOME for example
      if @overlay_dir == @tmp_overlay_dir and File.directory? @tmp_overlay_dir and @tmp_overlay_dir =~ /^\/tmp\//
        require 'fileutils'
        FileUtils.rm_rf @tmp_overlay_dir
      end
    end

    def env(envvar)
      @cached_env = {} unless @cached_env.is_a? Hash
      unless @cached_env.key? envvar
        value = %x(/usr/lib/portage/bin/portageq envvar #{envvar}).chomp
        @cached_env[envvar] = value
      end
      @cached_env[envvar]
    end

    def ebuild_exists(name, gem_version)
      ename = GGem.ebuild_name(name)
      portage_dir = File.join(@portage_dev_ruby,ename)
      overlay_dir = File.join(@rubydev_overlay,ename)
      version_requirement = gem_version.is_a?(Gem::Version::Requirement) ? gem_version : Gem::Version::Requirement.new(gem_version || ">= 0")
      ebuilds = Dir.glob(File.join(portage_dir,'*.ebuild')) + Dir.glob(File.join(overlay_dir,'*.ebuild'))
      return false if ebuilds.empty?
      ebuilds.map! {|e| File.basename(e).gsub(/\.ebuild$/,'') }
      satisfied_ebuild = ebuilds.find do |e|
        dep_atom = DependencyAtom.new('='+e, false)
        version = Gem::Version.create(dep_atom.version)
        version_requirement.satisfied_by?(version)
      end
      satisfied_ebuild != nil
    end
  end
end # class ConfigFile
