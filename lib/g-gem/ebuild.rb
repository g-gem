require 'g-gem/portage'
require 'g-gem/depatom'
require 'rubygems'

class Ebuild

  EXTENSION = ".ebuild"

  require 'forwardable'
  extend Forwardable

  def_delegators :@spec, :name, :version, :date, :summary, :email,
                 :rubyforge_project, :description, :local?, :remote?

  attr_reader :spec

  def initialize(spec)
    @spec = spec
  end

  def ebuild_name
    GGem.ebuild_name(spec.name)
  end

  def file_path
    File.expand_path(File.join(Portage.rubydev_overlay, ebuild_name, "#{ebuild_name}-#{spec.version}#{EXTENSION}"))
  end

  def dirname
    File.dirname(file_path)
  end

  def basename(include_extension=true)
    File.basename(file_path, (EXTENSION unless include_extension) )
  end

  def exists?
    File.exists? file_path
  end

  def write
    require 'fileutils'
    dir = File.dirname(file_path)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)
    File.open(file_path, 'w') { |f| f.write(content) }
  end

  def src_uri
    uri = spec.fetched_from if spec.respond_to? :fetched_from
    spec.local? ? "#{uri}" : "#{uri}/gems/" + (ebuild_name == spec.name ? "${P}.gem" : "${MY_P}.gem")
  end

  def license
    "Unknown"
  end

  def keywords
    Portage.env('ARCH')
  end

  def homepage
    @spec.homepage || "http://rubyforge.org/projects/#{@spec.rubyforge_project or @spec.name}"
  end

  def iuse
    "#{'doc' if spec.has_rdoc}"
  end

  def depend
    category = Portage.category
    required_ruby = DependencyAtom.from_required_ruby_version(spec)
    spec.dependencies.inject(required_ruby) do |s,d|
      s + "\n" + DependencyAtom.from_gem_dependency(d, category).to_s
    end
  end

  def content
    require 'date'
    <<-EBUILD
# Copyright 1999-#{Date.today.year} Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit ruby gems

USE_RUBY="ruby18"
MY_P="#{spec.name}-${PV}"
DESCRIPTION=#{(summary.capitalize.chomp('.')+'.').inspect}
HOMEPAGE=#{homepage.inspect}
SRC_URI=#{src_uri.inspect}

LICENSE=#{license.inspect}
SLOT="0"
KEYWORDS=#{keywords.inspect}

IUSE=#{iuse.inspect}
DEPEND=#{depend.inspect.gsub('\n', "\n\t")}
    EBUILD
  end

  def execute(*commands)
    generate if commands.delete('generate')
    raise "Missing #{file_path}" unless exists?
    system("ebuild #{file_path} #{commands.join(' ')}") unless commands.empty?
  end

end
