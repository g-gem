require 'rubygems'

class DependencyAtom

  PREFIX_EXT_RE = /[!~]/
  PREFIX_RE = /(>=|<=|<|>|=)/
  CATEGORY_RE = /\w+-\w+/
  POSTFIX_RE = /\*/
  VERSION_RE = /(\d+(\.\d+)*)+/
  VERSION_EXT_RE = /[a-z]?(_(alpha|beta|pre|rc|p)\d+)?/
  PACKAGE_NAME_RE = /[a-zA-Z0-9_-]+/

  attr_reader :package_name, :version, :version_ext,
              :prefix, :prefix_ext, :postfix, :origin,
              :gem_name

  # if convert is true, use GGem.ebuild_name() to convert the name
  def initialize(str, convert = true)
    @origin = str
    @prefix_ext, str = $&, $' if /^#{PREFIX_EXT_RE}/ === str
    @prefix, str = $&, $' if /^#{PREFIX_RE}/ === str
    @category, str = $1, $' if /^(#{CATEGORY_RE})\// === str
    @postfix, str = $&, $` if /#{POSTFIX_RE}$/ === str
    @version, @version_ext, str = $1, $3, $` if /-#{VERSION_RE}(#{VERSION_EXT_RE})/ === str
    @gem_name = str
    str = GGem.ebuild_name(str) if convert == true
    raise "Invalid atom: #{@origin}" unless /^#{PACKAGE_NAME_RE}$/ === str
    @package_name = $&
  end
  private :initialize

  alias_method :name, :package_name
  attr_accessor :category

  def category
    @category or Portage.category
  end

  ##
  # FIXME: support other gentoo atom prefixes
  ##
  def gem_requirement
    return Gem::Version::Requirement.default unless version
    full_prefix = "#{prefix_ext}#{prefix}"
    gem_prefix = if /^(~>|>|>=|=|!=|<=|<)$/ === full_prefix
                   full_prefix
                 elsif not prefix
                   "="
                 end
    Gem::Version::Requirement.create(["#{gem_prefix} #{version}"])
  end

  def gem_dependency
    Gem::Dependency.new(package_name, [gem_requirement.to_s])
  end

  def to_s
    "#{prefix_ext}#{(prefix or "=") if version}#{category+"/" if category}#{package_name}"+
      "#{"-"+version+version_ext if version}#{postfix}"
  end

  class << self
    def from_s(str)
      if str =~ /(.*\/)?(\S+)\.gem$/ # From gem file
        atom = from_s($2)
        atom.instance_eval { @origin = str }
        atom
      else
        new(str)
      end
    end

    alias_method :parse, :from_s

    def parse_list(list)
      list.map { |str| parse(str) }
    end

    def from_gem_dependency(dep, category)
      # fixme:
      if dep.is_a? Array
        dep = Gem::Dependency.new(dep[0], dep[1])
      end
      prefix, version = *dep.version_requirements.instance_eval { @requirements.first }
      if prefix.to_s == '>' && version.to_s == '0.0.0'
        new("#{category}/#{dep.name}")
      elsif prefix.to_s == '='
        # This is a special case. I want to make sure that -r* versions are prefered
        # over the original versions
        # The case here is radiant, depending on rails-1.1.6 while we also have
        # rails-1.1.6-r1. Of course, it should use rails-1.1.6-r1
        new("#{prefix}#{category}/#{dep.name}-#{version}*")
      else
        new("#{prefix}#{category}/#{dep.name}-#{version}")
      end
    end

    def from_required_ruby_version(spec)
      spec.required_ruby_version.instance_eval do
        if @requirements != nil 
          prefix, version = @requirements.first
          version.to_s == '0.0.0' ? 'dev-lang/ruby' : "#{prefix}dev-lang/ruby-#{version}"
        else
          "dev-lang/ruby"
        end
      end
    end
  end
end  # class DependencyAtom
