require 'g-gem/portage'
require 'g-gem/depatom'
require 'g-gem/ebuild'

module GGem
  @@gri = nil
  @@sources = nil

  SHORTMAP = {
    'p' => :pretend,
    'h' => :help,
    'v' => :verbose,
  }
  
  LONGMAP = {
    'prepend' => :pretend,
    'verbose' => :verbose,
    'help' => :help,
    'generate' => :generate,
    'madgen' => :mass_generate,
  }
  LONGMAP1 = {
    'source' => Proc.new do |param|
      Gem.sources.clear
      Gem.sources << param
    end,
    'add-source' => Proc.new do |param|
      Gem.sources << param
    end,
  }


  SPECIAL_MAPS = {
    'BlueCloth' => 'bluecloth',
    'KirbyBase' => 'kirbybase',
    'libxml-ruby' => 'libxml',
    'RedCloth' => 'redcloth',
    'mega' => 'mega-modules',
    'nano' => 'nano-methods',
    'gettext' => 'ruby-gettext',
    'snmp' => 'snmplib',
    'ZenTest' => 'zentest',
    'NGSLib-RFile' => 'ngslib-rfile',
    'tRuTag' => 'trutag',
  }

  class << self

    attr_accessor :options

    def main
      require_gem('sources')
      @options, packages = parse_opts(ARGV)
      Portage.setup_overlay(options[:pretend] || options[:mass_generate])

      if options[:mass_generate] == true
        mass_generate
        puts "Ebuilds are in #{Portage.rubydev_overlay}"
	return
      end

      if options[:help] == true
        puts <<EOF
g-gem syntax is similar to emerge one. You can use g-gem just as emerge.
The only difference is that you have to pass Gem names to it instead of
ebuild names. They will be treated as gems from remote sources.
You can also pass a local path or uri of a specific gem.

Generated ebuild names, however, can be different from
their original gem names to conform ebuild naming policies.
There are several g-gem specific options:

  --generate
        Only generate ebuilds (with digests so it will download gems)
	The first writable overlay will be used to store ebuilds.
	If no suitable overlay is found, a temporary overlay will be
	created in /tmp

  --madgen
        Generate ebuilds for *every* possible gems
        (which means at least all gems in gems.rubyforge.org)
        This option don't digest ebuilds (as a result, it does
        not fetch the real gems). You have to digest the ebuilds
        yourself if you want to use them.

  --prepend, -p
        Don't actually generate ebuilds. You see it as if you
        do "emerge --pretend". However the generated ebuilds will
        be removed right after "emerge --pretend" is done. Your
	overlays should be untouched

  --source URL
        Use URL as remote source for gems (default sources are ignored)

  --add-source URL
        Use URL as remote source for gems (default sources are kept)
EOF
        return
      end

      spec_groups = packages.map {|pkg| get_specs(pkg)}
      all_specs = spec_groups.flatten

      ebuilds = []
      for spec in all_specs
        puts "Create ebuild for #{spec.full_name}" if options[:verbose]
        gem_ebuild = Ebuild.new(spec)
        gem_ebuild.write
        ebuilds << gem_ebuild
      end
      # No prepend, digest the ebuilds
      if options[:pretend] == false
        ebuilds.each {|ebuild| ebuild.execute('digest')}
      end
      # don't specify --generate, continue digest and emerge
      if options[:generate] == false 
	cmd = 'emerge '+gen_opts(options)+' '+packages.map{|pkg| pkg.to_s.inspect}.join(' ')
	puts cmd if options[:verbose]
        system(cmd)
      end

      # don't clean up if --generate because overlay may be /tmp/*
      if options[:generate] == true
        puts "Ebuilds are in #{Portage.rubydev_overlay}"
      else
        Portage.cleanup
      end
    end

    def get_specs(pkg)
      specs = []
      if pkg.origin =~ /\.gem$/
	if File.exists? pkg.origin
          spec = Gem::Format.from_file_by_path(pkg.origin).spec
	else
	  require 'open-uri'
	  gem_file = open(pkg.origin)
	  spec = gem_file != nil ? Gem::Format.from_io(gem_file).spec : nil
	end
	return nil if spec.nil?
        class << spec
          def local?() true; end
	  def fetched_from() @source; end
        end
	spec.instance_eval { @source = pkg.origin }
        specs << spec
      else
        navigate(pkg.gem_name,pkg.gem_requirement) { |spec| specs << spec }
        specs.reverse
      end
    end

    def navigate(gem_name,gem_version = nil,&block)
      if Portage.ebuild_exists(gem_name,gem_version)
        puts "Ebuild for #{gem_name} exists" if options[:verbose]
        return true 
      end
      # cache source_index_hash, no need to recheck it everytime
      @@gri = Gem::RemoteInstaller.new if @@gri.nil?
      @@sources = @@gri.source_index_hash if @@sources.nil?
      version_requirement = gem_version.is_a?(Gem::Version::Requirement) ? gem_version : Gem::Version::Requirement.new(gem_version || ">= 0")
      gem_spec = nil
      for source_name,source in @@sources
        specs = source.find_name(gem_name, version_requirement)
        next if specs.nil? or specs.empty?
        # ignore mswin32 platform
        satisfied_specs = specs.find_all do |spec|
          spec.platform == 'ruby' and
            version_requirement.satisfied_by?(spec.version)
        end
        gem_spec = satisfied_specs[-1]
        class << gem_spec
          def local?() false; end
          def set_source(source)
            @source = source
          end
          def fetched_from
            @source
          end
        end
        gem_spec.set_source(source_name)
        break
      end
      if gem_spec.nil?
        puts "#{gem_name} not found"
        return false
      end
      yield(gem_spec)
      for dep in gem_spec.dependencies
        navigate(dep.name, dep.requirement_list, &block)
      end
      true
    end

    def gen_opts(opts)
      excludes = [:mass_generate, :generate]
      opts.keys.map do |key|
        opts[key] == true && !excludes.member?(key) ? "--#{key}" : ''
      end.join(' ')
    end

    def parse_opts(argv)
      opts = {
        :pretend => false,
        :generate => false,
        :help => false,
        :verbose => false,
        :mass_generate => false,
      }
      packages = []
    
      queued_option = nil

      for arg in argv
	if queued_option != nil
	  if LONGMAP1[queued_option].is_a? Symbol
	    opts[queued_option] = arg
          elsif LONGMAP1[queued_option].is_a? Proc
            LONGMAP1[queued_option].call arg
          else
            puts "Wrong parameter specification #{queued_option}"
          end
	  queued_option = nil
        elsif arg =~ /^-[^-]/
          for i in 1..arg.length-1
            if SHORTMAP.key? arg[i..i]
              opts[SHORTMAP[arg[i..i]]] = true
            else
              puts "Unknown option '-#{arg[i..i]}'"
            end
          end
        elsif arg =~ /^--/
          if LONGMAP.key? arg[2..-1]
            opts[LONGMAP[arg[2..-1]]] = true
          elsif LONGMAP1.key? arg[2..-1]
            queued_option = arg[2..-1]
          else
            puts "Unknown option '#{arg}'"
          end
        else
	  packages << DependencyAtom.from_s(arg)
        end
      end
      return opts, packages
    end

    def mass_generate
      @@gri = Gem::RemoteInstaller.new if @@gri.nil?
      @@sources = @@gri.source_index_hash if @@sources.nil?
      for source_name,source in @@sources
	source.each do |full_name,spec|
	  if spec.platform == 'ruby'
	    puts full_name
            class << spec
              def local?() false; end
              def set_source(source) @source = source end
              def fetched_from() @source end
            end
            spec.set_source(source_name)
	    e = Ebuild.new(spec)
	    e.write
	  end
	end
      end
    end

    def ebuild_name(gem_name)
      return SPECIAL_MAPS[gem_name] if SPECIAL_MAPS.key? gem_name
      # Remove punctation
      gem_name.gsub(/[_:.]+/,'-').
        gsub(/([^-0-9A-Z])([A-Z])([a-z])/) { "#{$1}-#{$2}#{$3}" }. # split words
        gsub(/([^-0-9A-Z])([A-Z])([a-z])/) { "#{$1}-#{$2}#{$3}" }. # split words again
        gsub(/([A-Z]{2,})([A-Z][^A-Z-])/) { "#{$1}-#{$2}"}. # find acronyms
        gsub(/([A-Z]{2,})-([A-Z])$/) {"#{$1}#{$2}"}. # recover wrong acronym breaking
        downcase
    end
  end
end
